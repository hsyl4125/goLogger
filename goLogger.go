package goLogger

//RFC5424
//SYSLOG-MSG = 优先级 版本 空格 时间戳 空格 主机名 空格 应用名 空格 进程id 空格 信息id

import (
	"errors"
	"fmt"
	"path"
	"runtime"
	"strings"
)

type LogLevel uint16

const (
	UNKNOWN LogLevel = iota
	DEBUG
	INFO
	WARN
	ERROR
)

type Logger interface {
	Debug(format string, a ...interface{})
	Info(format string, a ...interface{})
	Warn(format string, a ...interface{})
	Error(format string, a ...interface{})
}

func GetInfo(skip int) (funcName, fileName string, LineNum int) {
	pc, file, line, ok := runtime.Caller(skip)
	if !ok {
		fmt.Println("runtime.Caller(),failed")
		return
	}
	funcName = runtime.FuncForPC(pc).Name()
	fileName = path.Base(file)
	LineNum = line
	return
}

//将日志等级字符串转换为可运算的数字变量
//返回值：日志等级，错误消息
func parseLoglevel(s string) (LogLevel, error) {
	s = strings.ToLower(s)
	switch s {
	case "debug":
		return DEBUG, nil
	case "info":
		return INFO, nil
	case "warn":
		return WARN, nil
	case "error":
		return ERROR, nil
	case "unknown":
		return UNKNOWN, nil
	default:
		err := errors.New("unknown log level!")
		return UNKNOWN, err
	}
}

//映射日志数字到字符串
func unparseLogLevel(l LogLevel) (string, error) {
	switch l {
	case UNKNOWN:
		return "UNKNOWN", nil
	case DEBUG:
		return "DEBUG", nil
	case INFO:
		return "INFO", nil
	case WARN:
		return "WARN", nil
	case ERROR:
		return "ERROR", nil
	default:
		panic("The logLevel is error!")

	}
}
