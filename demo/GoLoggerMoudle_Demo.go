package main

import (
	"fmt"

	"gitee.com/hsyl4125/goLogger"
)

//var logger mylogger.Logger

//测试我自己写的mylogger日志库
func main() {
	fileLog, err := goLogger.NewFileLogger("INFO", "golang测试.log", "./", 1024000)
	if err != nil {
		fmt.Printf("日志结构体构造函数返回错误信息：%+v", err)
	}
	for i := 0; i < 10000; i++ {
		fileLog.Info("测试日志")
		fileLog.Warn("Warn级别日志")
		fileLog.Debug("debug级别日志，这是一条测试日志")
		fileLog.Error("这是一条error级别日志")
		fileLog.Error("这是一条error级别日志")
	}
	fileLog.CloseLogFile()
}
